fn hello_heap() {
    // Hello world using String constructor & the heap (memory allocation)
    let mut s = String::from("Hello");
    s.push_str(" heap!");
    println!("{}", s);
}

fn hello_stack() {
    // Hello world using String constructor & the stack (no memory allocation)
    let s = "Hello stack!";
    println!("{}", s);
}

fn hello_scope() {
    let s1 = String::from("Hello scope!");
    let s2 = s1; // pointer, length & capacity are copied out of s1
                 // the heap data (content of the string is not)

    // println!("{}", s1);  // invalid as s1 has been invalidated as soon as
                            // it has been affected to s2
    println!("{}", s2);
}

fn hello_deep_copy() {
    let s1 = String::from("Hello deep copy!");
    let s2 = s1.clone(); // pointer, length, capacity & heap data copied
                         // from s1 to s2
    println!("s1 = {} ; s2 = {}", s1, s2); // so we can access both contents
}

fn main() {
    hello_heap();
    hello_stack();
    hello_scope();
    hello_deep_copy();
}
