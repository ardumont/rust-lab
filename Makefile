MAKEFLAGS += --silent

dev:
	nix develop

version:
	rustc --version && cargo --version

new/%:
	cargo new $*

build/%:
	cd $* && cargo build

check/%:
	cd $* && cargo check

run-dev/%:
	cd $* && cargo run

run/%:
	./$*/target/debug/$*

release/%:
	cd $* && cargo build --release
