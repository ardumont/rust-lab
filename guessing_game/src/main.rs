use rand::Rng;
use std::cmp::Ordering;
use std::io;

fn main() {
    // Guessing game: Randomly select a number and let the user guess it.
    let secret: u8 = rand::rng().random_range(1..=100);
    let mut count: u8 = 0;
    'guessing: loop {
        println!("Guess the number x in interval [1:100]");

        let mut guess = String::new();
        println!("Please input a number: ");

        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line");

        let guess: u8 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        count += 1;

        match guess.cmp(&secret) {
            Ordering::Less => println!("x > {}", guess),
            Ordering::Greater => println!("x < {}", guess),
            Ordering::Equal => {
                println!("You found {} in {} round(s)!", secret, count);
                break 'guessing;
            }
        }
    }
}
