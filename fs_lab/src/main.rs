use id3::{Tag, TagLike, Version};

#[derive(Debug)]
struct MediaTag {
    year: i32,
    track: u32,
    title: String,
    album: String,
    artist: String,
}

impl MediaTag {
    fn write(&self, path: &str) {
        let mut tag = Tag::new();

        tag.set_track(self.track.try_into().unwrap());
        tag.set_year(self.year);
        tag.set_artist(&self.artist);
        tag.set_title(&self.title);
        tag.set_album(&self.album);

        match tag.write_to_path(path, Version::Id3v24) {
            Ok(_)  => {
                println!("Ok then, written to path: {}", path);
                return ()
            },
            Err(e) => {
                eprintln!("Application error: {e}");
                return ()
            }
        }
    }

    fn read(path: &str) -> Result<MediaTag, Box<dyn std::error::Error>> {
        let tag = match Tag::read_from_path(path) {
            Ok(tag) => tag,
            // Err(Error{kind: ErrorKind::NoTag, ..}) => return Tag::new(),
            Err(err) => return Err(Box::new(err)),
        };

        // Get a bunch of frames...
        let track = match tag.track() {
            Some(val) => val,
            None => todo!(), // return Err("File has no metadata track information."),
        };

        let year = match tag.year() {
            Some(val) => val,
            None => todo!(),
        };

        let artist = match tag.artist() {
            Some(val) => val.trim().to_string(),
            None => todo!(),
        };

        let title = match tag.title() {
            Some(val) => val.trim().to_string(),
            None => todo!(),
        };

        let album = match tag.album() {
            Some(val) => val.trim().to_string(),
            None => todo!(),
        };

        Ok(MediaTag {
            track,
            year,
            title,
            album,
            artist,
        })
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let title = String::from("Blame it on the love of rock'n'roll");

    let media_tag_temp = MediaTag {
        track: 9,
        year: 1982,
        title: title.clone(),
        album: title.clone(),
        artist: String::from("ardumont"),
    };

    media_tag_temp.write("/var/tmp/media.mp3");

    println!("Write media tag: {:#?}", media_tag_temp);

    let media_tag_read = MediaTag::read("/var/tmp/media.mp3");

    println!("Read media tag: {:#?}", media_tag_read);

    Ok(())
}
