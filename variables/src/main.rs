mod sample;

fn main () {
    sample::mutability::sample();
    sample::constants::sample();
    sample::shadow::sample();
}
