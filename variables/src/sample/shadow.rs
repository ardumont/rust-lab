pub fn sample() {
    println!("* Shadowing sample *");
    let x = 9;
    println!("Main block, first assignment: x = {}", x);
    let x = x + 1;
    println!("Shadow x in main block: x = {}", x);
    {
        let x = x*2;
        println!("inner block: x = {}", x);
    }
    println!("Shadow x in main block after all assignment are done: x = {}", x);
}

