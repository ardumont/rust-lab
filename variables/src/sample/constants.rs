const THREE_HOURS_IN_SECOND: u32 = 3 * 60 * 60;

pub fn sample() {
    println!("* Constants sample *");
    println!("THREE_HOURS_IN_SECOND = {}", THREE_HOURS_IN_SECOND);
}

